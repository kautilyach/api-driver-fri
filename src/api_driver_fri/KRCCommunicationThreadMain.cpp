//  ---------------------- Doxygen info ----------------------
//! \file KRCCommunicationThreadMain.cpp
//!
//! \brief
//! Implementation file for the thread of the class FastResearchInterface that
//! communicates with the KRC unit
//!
//! \details
//! The class FastResearchInterface provides a basic low-level interface
//! to the KUKA Light-Weight Robot IV For details, please refer to the file
//! FastResearchInterface.h.
//!
//! \date December 2014
//!
//! \version 1.2
//!
//!	\author Torsten Kroeger, tkr@stanford.edu\n
//! \n
//! Stanford University\n
//! Department of Computer Science\n
//! Artificial Intelligence Laboratory\n
//! Gates Computer Science Building 1A\n
//! 353 Serra Mall\n
//! Stanford, CA 94305-9010\n
//! USA\n
//! \n
//! http://cs.stanford.edu/groups/manips\n
//! \n
//! \n
//! \copyright Copyright 2014 Stanford University\n
//! \n
//! Licensed under the Apache License, Version 2.0 (the "License");\n
//! you may not use this file except in compliance with the License.\n
//! You may obtain a copy of the License at\n
//! \n
//! http://www.apache.org/licenses/LICENSE-2.0\n
//! \n
//! Unless required by applicable law or agreed to in writing, software\n
//! distributed under the License is distributed on an "AS IS" BASIS,\n
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n
//! See the License for the specific language governing permissions and\n
//! limitations under the License.\n
//!
//  ----------------------------------------------------------
//   For a convenient reading of this file's source code,
//   please use a tab width of four characters.
//  ----------------------------------------------------------

#include <FastResearchInterface.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <cmath>
#include <UDPSocket.h>
#include <FRICommunication.h>
#include <OSAbstraction.h>

// ****************************************************************
// KRCCommunicationThreadMain()
//
void* FastResearchInterface::KRCCommunicationThreadMain(void* ObjectPointer) {
    FastResearchInterface* ThisObject = (FastResearchInterface*)ObjectPointer;

    ThisObject->initCommunicationThread();

    pthread_mutex_lock(&(ThisObject->MutexForThreadCreation));
    ThisObject->ThreadCreated = true;
    pthread_mutex_unlock(&(ThisObject->MutexForThreadCreation));

    pthread_cond_signal(&(ThisObject->CondVarForThreadCreation));

    while (ThisObject->processCommunicationThread())
        ;

    ThisObject->endCommunicationThread();

    pthread_exit(NULL);

    return (NULL);
}

bool FastResearchInterface::initCommunicationThread() {
    static const float ZeroVector[] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f};

    int ResultValue = 0;

    thread_data_.KRC = new UDPSocket(ServerPort);

    thread_data_.LocalCommandData.SharedKRLVariables.FRIIntegerValuesInKRC[0] =
        0;
    thread_data_.LocalCommandData.SharedKRLVariables.FRIIntegerValuesInKRC[1] =
        0;

    for (;;) {
        // receive data from the KRC unit
        ResultValue = thread_data_.KRC->ReceiveFRIDataFromKRC(
            &thread_data_.LocalReadData);

        if (ResultValue != 0) {
            OutputConsole->printf(
                "FastResearchInterface::initCommunicationThread(): ERROR "
                "during the reception of a UDP data package.\n");
            return false;
        }

        thread_data_.LocalCommandData = CommandData;

        thread_data_.SequenceCounter++;
        thread_data_.LocalCommandData.Header.FRISequenceCounterForUDPPackages =
            thread_data_.SequenceCounter;
        thread_data_.LocalCommandData.Header
            .FRIReflectedSequenceCounterForUDPPackages =
            thread_data_.LocalReadData.Header.FRISequenceCounterForUDPPackages;
        thread_data_.LocalCommandData.Header.FRIDatagramID =
            FRI_DATAGRAM_ID_CMD;
        thread_data_.LocalCommandData.Header.FRIPackageSizeInBytes =
            sizeof(FRIDataSendToKRC);

        if (thread_data_.LocalReadData.SharedKRLVariables
                .FRIIntegerValuesInKRC[0] == 1) {
            thread_data_.LocalCommandData.SharedKRLVariables
                .FRIIntegerValuesInKRC[0] = 1;
            thread_data_.LocalCommandData.SharedKRLVariables
                .FRIIntegerValuesInKRC[1] = int(round(CycleTime * 1000.f));
        } else if (thread_data_.LocalReadData.SharedKRLVariables
                       .FRIIntegerValuesInKRC[0] == 2) {
            thread_data_.LocalCommandData.SharedKRLVariables
                .FRIIntegerValuesInKRC[0] = 0;
            thread_data_.LocalCommandData.SharedKRLVariables
                .FRIIntegerValuesInKRC[1] = 0;
        } else if (thread_data_.LocalReadData.SharedKRLVariables
                       .FRIIntegerValuesInKRC[0] == 3) {
            break;
        }

        // send data to KRC unit
        ResultValue =
            thread_data_.KRC->SendFRIDataToKRC(&thread_data_.LocalCommandData);

        // send data to KRC unit
        ResultValue =
            thread_data_.KRC->SendFRIDataToKRC(&thread_data_.LocalCommandData);

        if (ResultValue != 0) {
            OutputConsole->printf(
                "FastResearchInterface::initCommunicationThread(): ERROR "
                "during the sending of a UDP data package.\n");
            return false;
        }
    }

    return true;
}

bool FastResearchInterface::processCommunicationThread() {
    static const float ZeroVector[] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f};

    int ResultValue = 0;

    // receive data from the KRC unit
    ResultValue =
        thread_data_.KRC->ReceiveFRIDataFromKRC(&thread_data_.LocalReadData);

    if (ResultValue != 0) {
        OutputConsole->printf(
            "FastResearchInterface::processCommunicationThread(): ERROR during "
            "the reception of a UDP data package.\n");
        return false;
    }

    if (CurrentControlScheme == FastResearchInterface::JOINT_TORQUE_CONTROL or
        CurrentControlScheme == FastResearchInterface::JOINT_DYNAMIC_CONTROL) {
        SetCommandedJointDamping(ZeroVector);
        SetCommandedJointStiffness(ZeroVector);
    }

    pthread_mutex_lock(&(MutexForControlData));

    NewDataFromKRCReceived = true;
    ReadData = thread_data_.LocalReadData;

    if (!(KRCCommunicationThreadIsRunning)) {
        pthread_mutex_unlock(&(MutexForControlData));
        return false;
    }

    thread_data_.LocalCommandData = CommandData;

    thread_data_.SequenceCounter++;
    thread_data_.LocalCommandData.Header.FRISequenceCounterForUDPPackages =
        thread_data_.SequenceCounter;
    thread_data_.LocalCommandData.Header
        .FRIReflectedSequenceCounterForUDPPackages =
        thread_data_.LocalReadData.Header.FRISequenceCounterForUDPPackages;
    thread_data_.LocalCommandData.Header.FRIDatagramID = FRI_DATAGRAM_ID_CMD;
    thread_data_.LocalCommandData.Header.FRIPackageSizeInBytes =
        sizeof(FRIDataSendToKRC);

    pthread_mutex_unlock(&(MutexForControlData));

    pthread_cond_broadcast(&(CondVarForDataReceptionFromKRC));

    // send data to KRC unit
    ResultValue =
        thread_data_.KRC->SendFRIDataToKRC(&thread_data_.LocalCommandData);

    if (ResultValue != 0) {
        OutputConsole->printf(
            "FastResearchInterface::processCommunicationThread(): ERROR during "
            "the sending of a UDP data package.\n");
        return false;
    }

    return true;
}

bool FastResearchInterface::stopCommunicationThread() {
    int ResultValue = 0;

    // Bad workaround for the KUKA friUDP class, which unfortunately
    // does not use select() for sockets in order to enable timeouts.
    //
    // For the case, no UDP connection to the KRC could be
    // established, we have to wake up the communication thread
    // by sending a signal as it is still waiting for a message
    // from the KRC.

    pthread_mutex_lock(&(this->MutexForControlData));
    this->NewDataFromKRCReceived = false;
    pthread_mutex_unlock(&(this->MutexForControlData));

    delay(100 + 2 * (unsigned int)(this->CycleTime));

    pthread_mutex_lock(&(this->MutexForControlData));
    if (this->NewDataFromKRCReceived) {
        pthread_mutex_unlock(&(this->MutexForControlData));

        if (this->IsMachineOK()) {
            this->StopRobot();
        }

        // The KRL program running on the robot controller waits for a
        // integer value change at position 15 (i.e., 16 in KRL).
        // A value of 30 or 40 (depending on the value of
        // this->GetKRLIntValue(14), that is, $FRI_TO_INT[15]
        // lets the KRL program call friClose() and
        // terminate itself.

        this->SetKRLIntValue(15, this->GetKRLIntValue(14));

        // wait for the next data telegram of the KRC unit
        pthread_mutex_lock(&(this->MutexForControlData));
        this->NewDataFromKRCReceived = false;
        pthread_mutex_unlock(&(this->MutexForControlData));
        ResultValue =
            this->WaitForKRCTick(((unsigned int)(this->CycleTime * 3000000.0)));

        if (ResultValue != EOK) {
            this->OutputConsole->printf(
                "FastResearchInterface::stopCommunicationThread(): ERROR, the "
                "KRC unit does not respond anymore. Probably the FRI was "
                "closed or there is no UDP connection anymore.");
            getchar();
            if (has_communication_thread_been_created_) {
                pthread_kill(this->KRCCommunicationThread, SIGTERM);
            }
            return false;
        }

        // wait until the KRL program ended
        // (no UDP communication possible anymore)
        delay(800);

        // End the KRC communication thread
        // pthread_mutex_unlock(&(this->MutexForControlData));
        // The UDP communication is working --> normal shutdown
        this->KRCCommunicationThreadIsRunning = false;
        // pthread_mutex_unlock(&(this->MutexForControlData));
    } else {
        // No UDP communication to KRC and the communication
        // thread is blocked --> we wake him up manually
        this->KRCCommunicationThreadIsRunning = false;
        pthread_mutex_unlock(&(this->MutexForControlData));
        if (has_communication_thread_been_created_) {
            pthread_kill(this->KRCCommunicationThread, SIGTERM);
        }
    }

    if (has_communication_thread_been_created_) {
        pthread_join(this->KRCCommunicationThread, NULL);
    }

    delete[] this->RobotStateString;
    delete this->OutputConsole;

    return true;
}

bool FastResearchInterface::endCommunicationThread() {
    delete thread_data_.KRC;
    return true;
}
