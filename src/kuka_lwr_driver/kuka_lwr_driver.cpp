/*  File: kuka_lwr_driver_fri.cpp
 *	This file is part of the program kuka-lwr-driver-fri
 *      Program description : A package to interface with a real Kuka LWR using
 *the FRI library Copyright (C) 2015 -  Benjamin Navarro (LIRMM) Robin Passama
 *(LIRMM). All Right reserved.
 *
 *	This software is free software: you can redistribute it and/or modify
 *	it under the terms of the CeCILL-C license as published by
 *	the CEA CNRS INRIA, either version 1
 *	of the License, or (at your option) any later version.
 *	This software is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	CeCILL-C License for more details.
 *
 *	You should have received a copy of the CeCILL-C License
 *	along with this software. If not, it can be found on the official website
 *	of the CeCILL licenses family (http://www.cecill.info/index.en.html).
 */
/**
 * @file kuka_lwr_driver_fri.cpp
 * @author Benjamin Navarro (main author)
 * @author Robin Passama (refactoring)
 * @brief implemntation file for the knowbotics Kuka LWR FRI Driver
 * @date February 2015 02.
 */

#include <fri/kuka_lwr_driver.h>
#include <LWRBaseControllerInterface.h>
#include <OSAbstraction.h>

#include <unistd.h>
#include <iostream>

namespace fri {

KukaLWRDriver::KukaLWRDriver(KukaLWRRobot& robot, uint8_t command_modes,
                             double cycle_time, int udp_port,
                             bool start_fri_communication_thread,
                             bool enable_fri_logging)
    : robot_(robot),
      enabled_control_modes_(command_modes),
      current_control_mode_(MonitorMode),
      robot_in_monitor_mode_(false),
      cycle_time_(cycle_time),
      udp_port_(udp_port),
      start_fri_communication_thread_(start_fri_communication_thread),
      enable_fri_logging_(enable_fri_logging) {
    std::cout << "Initializing the Kuka LWR..." << std::endl;
    ctrl_ = std::make_unique<LWRBaseControllerInterface>(
        cycle_time_, udp_port_, start_fri_communication_thread_,
        enable_fri_logging_);
    robot_in_monitor_mode_ = true;
    std::cout << "The Kuka LWR is now in monitor mode" << std::endl;
}

KukaLWRDriver::~KukaLWRDriver() = default;

bool KukaLWRDriver::init() {
    size_t enabled_mode_count = 0;
    for (size_t mode = CommandModes::JointPositionControl;
         mode <= CommandModes::CartesianImpedanceControl; mode <<= 1) {
        if (mode & enabled_control_modes_) {
            ++enabled_mode_count;
        }
    }

    // If only one mode is enabled, start it here. Otherwise, wait for a call to
    // set_Current_Mode()
    if (enabled_mode_count == 1) {
        set_Current_Mode(static_cast<CommandModes>(enabled_control_modes_));
    }

    return true;
}

bool KukaLWRDriver::init_Communication_Thread() {
    return ctrl_->GetFRI().initCommunicationThread();
}

bool KukaLWRDriver::process(bool synchro) {
    bool all_ok = true;

    all_ok &= get_Data(synchro);
    all_ok &= send_Data();

    return all_ok;
}

bool KukaLWRDriver::process_Communication_Thread() {
    return ctrl_->GetFRI().processCommunicationThread();
}

bool KukaLWRDriver::end() {
    bool ret = true;
    if (is_Robot_Started())
        ret = stop_Robot();

    robot_in_monitor_mode_ = false;

    return ret;
}

bool KukaLWRDriver::end_Communication_Thread() {
    bool all_ok = true;
    all_ok &= ctrl_->GetFRI().stopCommunicationThread();
    all_ok &= ctrl_->GetFRI().endCommunicationThread();
    return all_ok;
}

double KukaLWRDriver::get_Cycle_Time() const {
    return cycle_time_;
}

bool KukaLWRDriver::set_Current_Mode(CommandModes mode, bool silent) {
    bool ok = false;
    if ((mode != current_control_mode_) and (mode & enabled_control_modes_)) {
        if (is_Robot_Started() and not stop_Robot()) {
            std::cout << "Can't set the command mode for the Kuka LWR"
                      << std::endl;
        } else {
            current_control_mode_ = mode;
            if (is_Robot_Started()) {
                ok = start_Robot(silent);
            }
            if (ok) {
                get_Data(true);
            }
        }
    }
    return ok;
}

KukaLWRDriver::CommandModes KukaLWRDriver::get_Current_Mode() const {
    return current_control_mode_;
}

bool KukaLWRDriver::is_Robot_OK() const {
    return (ctrl_ and ctrl_->IsMachineOK());
}

bool KukaLWRDriver::is_Robot_Started() const {
    return current_control_mode_ != MonitorMode;
}

bool KukaLWRDriver::is_Robot_In_Monitor_Mode() const {
    return robot_in_monitor_mode_;
}

void KukaLWRDriver::wait_For_KRC_Tick() {
    if (ctrl_) {
        ctrl_->WaitForKRCTick();
    }
}

bool KukaLWRDriver::start_Robot(bool silent) {
    int ResultValue = 0;

    std::cout << "Starting the Kuka LWR..." << std::endl;

    switch (current_control_mode_) {
    case JointPositionControl:
        ResultValue = ctrl_->StartRobotInJointPositionControl();
        break;
    case JointImpedanceControl:
        ResultValue = ctrl_->StartRobotInJointImpedanceControl();
        break;
    case GravityCompensation:
        ResultValue = ctrl_->StartRobotInJointTorqueControl(); // TODO update
                                                               // FRI mode names
        break;
    case JointTorqueControl:
        ResultValue =
            ctrl_->StartRobotInJointDynamicControl(); // TODO update FRI mode
                                                      // names
        break;
    case CartesianImpedanceControl:
        ResultValue = ctrl_->StartRobotInCartesianImpedanceControl();
        break;
    default:
        break;
    }

    if (ResultValue == EOK) {
        std::cout << "Kuka LWR successfully started" << std::endl;
        do {
            ctrl_->WaitForKRCTick();
        } while (not ctrl_->IsMachineOK());
    } else {
        std::cout << "ERROR, could not start Kuka LWR: "
                  << strerror(ResultValue) << std::endl;
        current_control_mode_ = MonitorMode;
    }

    if (not silent) {
        std::cout << "Current system state:" << std::endl
                  << ctrl_->GetCompleteRobotStateAndInformation() << std::endl;
    }

    return is_Robot_Started();
}

bool KukaLWRDriver::stop_Robot() {
    bool ret = false;
    if (is_Robot_Started()) {
        wait_For_KRC_Tick();
        std::cout << "Stopping the Kuka LWR" << std::endl;
        if (ctrl_->StopRobot() != EOK) {
            std::cout << "An error occurred during stopping the Kuka LWR"
                      << std::endl;
        } else {
            std::cout << "Kuka LWR successfully stopped" << std::endl;
            ret = true;
        }
        current_control_mode_ = MonitorMode;
    }

    return ret;
}

bool KukaLWRDriver::get_Data(bool synchro) {
    float buffer[12];
    auto copy_from_buffer = [&buffer](auto& dest) {
        for (size_t i = 0; i < dest.size(); i++) {
            dest[i] = buffer[i];
        }
    };

    if (synchro) {
        ctrl_->WaitForKRCTick();
    }

    if (is_Robot_In_Monitor_Mode()) {
        // Get joint positions and set joint target positions
        if (ctrl_->GetMeasuredJointPositions(buffer) == EOK) {
            copy_from_buffer(robot_.state.joint_position);
        } else {
            std::cout << "Can't get joint positions from FRI" << std::endl;
            return false;
        }

        // Get joint torques
        if (ctrl_->GetMeasuredJointTorques(buffer) == EOK)
            copy_from_buffer(robot_.state.joint_torques);
        else {
            std::cout << "Can't get joint torques from FRI" << std::endl;
            return false;
        }

        // Get external joint torques
        if (current_control_mode_ == JointTorqueControl) {
            robot_.state.joint_external_torques.setZero();
        } else if (ctrl_->GetEstimatedExternalJointTorques(buffer) == EOK)
            copy_from_buffer(robot_.state.joint_external_torques);
        else {
            std::cout << "Can't get external joint torques from FRI"
                      << std::endl;
            return false;
        }

        // Get joint gravity torques
        if (current_control_mode_ == JointTorqueControl) {
            robot_.state.joint_gravity_torques.setZero();
        } else if (ctrl_->GetCurrentGravityVector(buffer) == EOK)
            copy_from_buffer(robot_.state.joint_gravity_torques);
        else {
            std::cout << "Can't get joint gravity torques from FRI"
                      << std::endl;
            return false;
        }

        // Get joint temperature
        ctrl_->GetFRI().GetDriveTemperatures(buffer);
        copy_from_buffer(robot_.state.joint_temperature);

        // Get TCP pose
        if (ctrl_->GetMeasuredCartPose(buffer) == EOK) {
            for (int row = 0; row < 3; ++row) {
                for (int col = 0; col < 4; ++col) {
                    robot_.state.tcp_pose(row, col) = buffer[col + 4 * row];
                }
            }
        } else {
            std::cout << "Can't get TCP pose from FRI" << std::endl;
            return false;
        }

        // Get end effector wrench
        if (current_control_mode_ == JointTorqueControl) {
            robot_.state.wrench.setZero();
        } else if (ctrl_->GetEstimatedExternalCartForcesAndTorques(buffer) ==
                   EOK) {
            // The array comming from FRI is [Fx, Fy, Fz, Tz, Ty, Tx]
            copy_from_buffer(robot_.state.wrench);
            robot_.state.wrench =
                -robot_.state.wrench; // The provided wrench is the one applied
                                      // to the environment, not what it is
                                      // applied to the robot
            std::swap(robot_.state.wrench(3), robot_.state.wrench(5));
        } else {
            std::cout << "Can't get force sensor values from FRI" << std::endl;
            return false;
        }

        // Get mass matrix
        {
            float mat[7][7];
            ctrl_->GetCurrentMassMatrix(mat);
            for (size_t i = 0; i < 7; i++) {
                for (size_t j = 0; j < 7; j++) {
                    robot_.state.mass_matrix(i, j) = mat[i][j];
                }
            }
        }

    } else {
        std::cout << "ERROR, the Kuka LWR is not ready anymore" << std::endl;
        return false;
    }

    return true;
}

bool KukaLWRDriver::send_Data() {
    float buffer[12];
    auto copy_to_buffer = [&buffer](const auto& dest) {
        for (size_t i = 0; i < dest.size(); i++) {
            buffer[i] = dest[i];
        }
    };

    if (is_Robot_OK() and is_Robot_Started()) {
        switch (current_control_mode_) {
        case JointImpedanceControl:
            copy_to_buffer(robot_.command.joint_stiffness);
            ctrl_->SetCommandedJointStiffness(buffer);

            copy_to_buffer(robot_.command.joint_damping);
            ctrl_->SetCommandedJointDamping(buffer);
            // fall-through
        case GravityCompensation:
            // fall-through
        case JointTorqueControl:
            copy_to_buffer(robot_.command.joint_torques);
            ctrl_->SetCommandedJointTorques(buffer);
            // fall-through
        case JointPositionControl:
            copy_to_buffer(robot_.command.joint_position);
            ctrl_->SetCommandedJointPositions(buffer);
            break;

        case CartesianImpedanceControl:
            for (int row = 0; row < 3; ++row) {
                for (int col = 0; col < 4; ++col) {
                    buffer[col + 4 * row] = robot_.command.tcp_pose(row, col);
                }
            }
            ctrl_->SetCommandedCartPose(buffer);

            // Set cartesian stiffness
            copy_to_buffer(robot_.command.tcp_stiffness);
            ctrl_->SetCommandedCartStiffness(buffer);

            copy_to_buffer(robot_.command.tcp_damping);
            ctrl_->SetCommandedCartDamping(buffer);

            break;
        default:
            break;
        }
    } else {
        std::cout << "ERROR, the Kuka LWR is not ready anymore (machine ok: "
                  << ctrl_->IsMachineOK()
                  << ", command mode: " << current_control_mode_ << ")"
                  << std::endl;
        return false;
    }

    return true;
}

} // namespace fri
