#include <fri/kuka_lwr_driver.h>

#include <iostream>

int main(int argc, char const* argv[]) {
    const double cycle_time = 0.005;

    fri::KukaLWRRobot robot;
    fri::KukaLWRDriver driver(robot, fri::KukaLWRDriver::JointImpedanceControl,
                              cycle_time,
                              49938 // local UDP port (see KRC configuration)
    );

    if (not driver.init()) {
        throw std::runtime_error(
            "Initialization of the Kuka LWR driver failed.");
    }

    std::cout << "robot.state.joint_position: "
              << robot.state.joint_position.transpose() << std::endl;

    robot.command.joint_position = robot.state.joint_position;
    robot.command.joint_stiffness.setConstant(100.);

    driver.send_Data();
    for (size_t i = 0; i < 10 / cycle_time; ++i) {
        driver.wait_For_KRC_Tick();
    }

    if (not driver.end()) {
        throw std::runtime_error(
            "The Kuka LWR driver couldn't be stopped properly.");
    }

    return 0;
}
