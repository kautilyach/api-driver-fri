/**
 * @file kuka_lwr_driver.h
 * @author Benjamin Navarro  (main author)
 * @brief include file for the Kuka LWR FRI driver
 * @date July 2018
 */

#pragma once

#include <fri/kuka_lwr_robot.h>

#include <cstdint>
#include <memory>

class LWRBaseControllerInterface;

/**
 * @brief FRI related data and drivers
 *
 * \ingroup fri
 */
namespace fri {

/**
 * Kuka LWR driver based on FRI.
 *
 * This driver handles multiple control modes and switching between them at run
 * time. Also, thanks to customized FRI library and KRC scripts, the cycle time
 * can be set from the driver directly.
 */
class KukaLWRDriver {
public:
    /*! Available command modes. Can be OR-ed together. */
    enum CommandModes : uint8_t {
        MonitorMode = 0, /*!< Can be used to only monitor the robot state
                            without sending any command. */
        JointPositionControl =
            1 << 0, /*!< KRC default joint position control mode. */
        JointImpedanceControl =
            1 << 1, /*!< KRC default joint impedance control. */
        GravityCompensation =
            1 << 2, /*!< Modified joint impedance control. Joint stiffness and
                       damping are forced to zero. */
        JointTorqueControl =
            1 << 3, /*!< Modified joint impedance control. Joint stiffness and
                       damping are forced to zero and the gravity compensation
                       is removed. */
        CartesianImpedanceControl =
            1 << 4, /*!< KRC default cartesian impedance control mode. */
        AllModes = (1 << 5) - 1 /*!< Pre-enable all control modes */
    };

    /**
     * @param robot         A reference to the robot to control
     * @param command_modes The pre-enabled control modes
     * @param cycle_time    The cycle time to set for the KRC
     * @param udp_port      The UDP port configured on the KRC. Find it using
     * FRI -> Show on the KCP.
     */
    KukaLWRDriver(KukaLWRRobot& robot, uint8_t command_modes, double cycle_time,
                  int udp_port, bool start_fri_communication_thread = true,
                  bool enable_fri_logging = true);

    virtual ~KukaLWRDriver();

    /**
     * Initialize the communication with the robot.
     * If a single control mode is passed to the constructor, set_Current_Mode()
     * will be called with this mode.
     * @return true on success, false otherwise.
     */
    virtual bool init();

    /**
     * Initialize the FRI communication thread with the KRC. To be used only
     * with start_fri_communication_thread = false.
     * @return true on success, false otherwise.
     */
    bool init_Communication_Thread();

    /**
     * Call sequentialy get_Data(synchro) and send_Data()
     * @param  synchro Call wait_For_KRC_Tick() before updating the robot state.
     * @return         true on success, false otherwise.
     */
    virtual bool process(bool synchro = false);

    /**
     * Run once cycle of the FRI communication thread. To be used only with
     * start_fri_communication_thread = false.
     * @return true if it sould be kept running, false otherwise.
     */
    bool process_Communication_Thread();

    /**
     * Close the communication with the robot.
     * @return true on success, false otherwise.
     */
    virtual bool end();

    /**
     * End the FRI communication thread with the KRC. To be used only with
     * start_fri_communication_thread = false.
     * @return true on success, false otherwise.
     */
    bool end_Communication_Thread();

    /**
     * Provide the cycle time set at construction
     * @return The sample time is seconds
     */
    double get_Cycle_Time() const;

    /**
     * Switch to a new control mode.
     * @param  mode   The new control mode
     * @param  silent Set to true to dump all the FRI state
     * @return        true on success, false otherwise.
     */
    bool set_Current_Mode(CommandModes mode, bool silent = false);

    /**
     * Give the control mode currently in use.
     * @return The control mode.
     */
    CommandModes get_Current_Mode() const;

    /**
     * Block the caller until the synchronization signal from the KRC is
     * received.
     */
    void wait_For_KRC_Tick();

    /**
     * Check if the robot is still ok to be used.
     * @return true if ok, false otherwise.
     */
    bool is_Robot_OK() const;

    /**
     * Tell if the robot has left monitor mode.
     * @return true if started, false otherwise.
     */
    bool is_Robot_Started() const;

    /**
     * Tell if the robot state can be monitored.
     * @return true if in monitor mode, false otherwise.
     */
    bool is_Robot_In_Monitor_Mode() const;

    /**
     * Update the robot state with the latest data from the KRC.
     * @param  synchro Call wait_For_KRC_Tick() before updating the robot state.
     * @return         true on success, false otherwise
     */
    virtual bool get_Data(bool synchro = false);

    /**
     * Send the robot commands to the KRC.
     * @return true on success, false otherwise
     */
    virtual bool send_Data();

private:
    bool start_Robot(bool silent = false);
    bool stop_Robot();

    KukaLWRRobot& robot_;
    uint8_t enabled_control_modes_;
    CommandModes current_control_mode_;
    bool robot_in_monitor_mode_;
    double cycle_time_;
    int udp_port_;
    bool start_fri_communication_thread_;
    bool enable_fri_logging_;

    std::unique_ptr<LWRBaseControllerInterface> ctrl_;
};

} // namespace fri
