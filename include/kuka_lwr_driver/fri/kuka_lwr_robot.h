#pragma once

#include <Eigen/Dense>
#include <Eigen/Geometry>

namespace fri {

/**
 * Structure holding all the robot related data (state and command)
 */
struct KukaLWRRobot {
    using JointVector = Eigen::Matrix<double, 7, 1>;
    using TCPVector = Eigen::Matrix<double, 6, 1>;
    using TCPTransform = Eigen::Affine3d;
    using MassMatrix = Eigen::Matrix<double, 7, 7>;

    /**
     * Structure holding the robot state
     */
    struct State {
        JointVector joint_position =
            JointVector::Zero(); /*!< joint positions in radians */
        JointVector joint_torques =
            JointVector::Zero(); /*!< joint torques in Nm */
        JointVector joint_external_torques =
            JointVector::Zero(); /*!< joint external torques in Nm */
        JointVector joint_gravity_torques =
            JointVector::Zero(); /*!< joint gravity torques in Nm */
        JointVector joint_temperature =
            JointVector::Zero(); /*!< joint temperature in °C */
        TCPTransform tcp_pose =
            TCPTransform::Identity();         /*!< TCP pose expressed as
                                                 an Eigen transform */
        TCPVector wrench = TCPVector::Zero(); /*!< TCP wrench (Fx, Fy, Fz, Tx,
                                                 Ty, Tz) in N,Nm */
        MassMatrix mass_matrix =
            MassMatrix::Zero(); /*!< (approximate) mass matrix */
    };

    /**
     * Structure holding the robot command
     */
    struct Command {
        JointVector joint_position =
            JointVector::Zero(); /*!< joint positions in radians */
        JointVector joint_torques =
            JointVector::Zero(); /*!< joint torques in Nm */
        JointVector joint_stiffness =
            JointVector::Constant(1000.); /*!< joint stiffness in Nm/rad */
        JointVector joint_damping =
            JointVector::Constant(0.7); /*!< joint damping (unitless, 0<d<1) */
        TCPTransform tcp_pose =
            TCPTransform::Identity(); /*!< TCP pose expressed as
                                         an Eigen transform */
        TCPVector tcp_stiffness =
            TCPVector((TCPVector() << Eigen::Vector3d::Constant(200.),
                       Eigen::Vector3d::Constant(20.))
                          .finished()); /*!< TCP stiffness (Ktx, Kty, Ktz, Krx,
                                           Kry, Krz) in N/m,Nm/rad */
        TCPVector tcp_damping =
            TCPVector::Constant(0.7); /*!< TCP damping (Dtx, Dty, Dtz, Drx, Dry,
                                         Drz) (unitless, 0<d<1) */
    };

    State state;     /*!< current robot state */
    Command command; /*!< current robot command */
};

} // namespace fri
