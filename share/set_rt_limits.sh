#!/bin/bash

echo -e "$USER\t-\trtprio\t98"  | sudo tee --append /etc/security/limits.conf
echo -e "$USER\t-\tmemlock\t-1" | sudo tee --append /etc/security/limits.conf
echo -e "$USER\t-\tnice\t-20"   | sudo tee --append /etc/security/limits.conf
